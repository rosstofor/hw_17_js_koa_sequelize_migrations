#### start
- docker-compose up

###### prestart ---> seeders with test data

##### routes
###### users
- GET http://localhost:3000/user
- GET http://localhost:3000/user/:id
- POST http://localhost:3000/user
    
    {
        "full_name":"Test testovich",
        "email":"test@test.com",
        "age": 25,
        "address_id":1
    }

- PUT http://localhost:3000/user/:id

    {
        "full_name":"Test testovich",
        "email":"test@test.com",
        "age": 25,
        "address_id":1
    }

- DELETE http://localhost:3000/user/:id

###### addresses
- GET http://localhost:3000/address
- GET http://localhost:3000/address/:id
- POST http://localhost:3000/address
    
    {
        "city":"Kiev",
        "street":"Kievska",
        "building":50
    }

- PUT http://localhost:3000/address/:id

    {
        "city":"Kiev2",
        "street":"Kievska2",
        "building":50
    }

- DELETE http://localhost:3000/address/:id
