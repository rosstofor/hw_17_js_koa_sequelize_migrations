"use strict";

const { Op } = require("sequelize");
const { Sequelize } = require("../models");
const users = [
  {
    full_name: "John Doe",
    email: "johndoe@test.com",
    age: 30,
    address_id: 1,
    role_id: 2,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    full_name: "Mickel Jackson",
    email: "jackson@test.com",
    age: 90,
    address_id: 2,
    role_id: 1,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    full_name: "Britney Spears",
    email: "spears@test.com",
    age: 45,
    address_id: 3,
    role_id: 3,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    full_name: "Test Testovich",
    email: "testovich@test.com",
    age: 99,
    address_id: 1,
    role_id: 3,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("User", users);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("User", {
      email: {
        [Op.in]: users.map((it) => it.email),
      },
    });
  },
};
