const { User, Role, Address, Permission } = require("../db/models");

const getUsersService = async () => {
  try {
    const data = await User.findAll({
      order: [["id", "ASC"]],
      attributes: ["full_name", "email", "age"],
      include: [
        {
          model: Role,
          attributes: ["name"],
          include: {
            model: Permission,
            attributes: ["name"],
          },
        },
        {
          model: Address,
          attributes: ["city", "street", "building"],
        },
      ],
    });
    return data;
  } catch (err) {
    return err;
  }
};
const getUserService = async (id) => {
  try {
    const data = await User.findOne({
      where: { id },
      attributes: ["full_name", "email", "age"],
      include: [
        {
          model: Role,
          attributes: ["name"],
          include: {
            model: Permission,
            attributes: ["name"],
          },
        },
        {
          model: Address,
          attributes: ["city", "street", "building"],
        },
      ],
    });
    return data;
  } catch (err) {
    return err;
  }
};
const createUserService = async (user) => {
  try {
    const data = await User.create({ ...user });
    return data;
  } catch (err) {
    return err;
  }
};
const updateUserService = async (user, id) => {
  try {
    const data = await User.update({ ...user }, { where: { id } });
    return data;
  } catch (err) {
    return err;
  }
};
const deleteUserService = async (id) => {
  try {
    const data = await User.destroy({ where: { id } });
    return data;
  } catch (err) {
    return err;
  }
};

const checkUserService = async (id) => {
  try {
    const data = await User.findOne({ where: { id } });
    return data;
  } catch (err) {
    return err;
  }
};

module.exports = {
  getUsersService,
  getUserService,
  createUserService,
  deleteUserService,
  updateUserService,
  checkUserService,
};
