const { Address, User } = require("../db/models");

const getAddressesService = async () => {
  try {
    const data = await Address.findAll({
      order: [["id", "ASC"]],
      include: {
        model: User,
        attributes: ["full_name", "email", "age"],
      },
    });
    return data;
  } catch (err) {
    return err;
  }
};
const getAddressService = async (id) => {
  try {
    const data = await Address.findOne({
      where: { id },
      include: {
        model: User,
        attributes: ["full_name", "email", "age"],
      },
    });
    return data;
  } catch (err) {
    return err;
  }
};
const createAddressService = async (address) => {
  try {
    const data = await Address.create({ ...address });
    return data;
  } catch (err) {
    return err;
  }
};
const updateAddressService = async (address, id) => {
  try {
    const data = await Address.update({ ...address }, { where: { id } });
    return data;
  } catch (err) {
    return err;
  }
};
const deleteAddressService = async (id) => {
  try {
    const data = await Address.destroy({ where: { id } });
    return data;
  } catch (err) {
    return err;
  }
};

const checkAddressService = async (id) => {
  try {
    const data = await Address.findOne({ where: { id } });
    return data;
  } catch (err) {
    return err;
  }
};
module.exports = {
  getAddressesService,
  getAddressService,
  createAddressService,
  deleteAddressService,
  updateAddressService,
  checkAddressService,
};
