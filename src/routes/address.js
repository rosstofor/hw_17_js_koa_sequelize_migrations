const Router = require("koa-router");
const {
  getAddress,
  updateAddress,
  createAddress,
  deleteAddress,
  getAddresses,
} = require("../controllers/address");

const router = new Router({ prefix: "/address" });

router
  .get("/", getAddresses)
  .get("/:id", getAddress)
  .post("/", createAddress)
  .put("/:id", updateAddress)
  .delete("/:id", deleteAddress);

module.exports = router;
