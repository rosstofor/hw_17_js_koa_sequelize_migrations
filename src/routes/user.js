const Router = require("koa-router");
const {
  getUsers,
  getUser,
  deleteUser,
  createUser,
  updateUser,
} = require("../controllers/user");

const router = new Router({ prefix: "/user" });

router
  .get("/", getUsers)
  .get("/:id", getUser)
  .post("/", createUser)
  .put("/:id", updateUser)
  .delete("/:id", deleteUser);

module.exports = router;
