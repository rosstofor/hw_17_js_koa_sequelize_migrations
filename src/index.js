require("dotenv").config();
const db = require("./db/models");
const Koa = require("koa");
const bodyParser = require("koa-bodyparser");
const { userRouter, addressRouter } = require("./routes");

const app = new Koa();

app.use(bodyParser());
app.use(userRouter.routes());
app.use(addressRouter.routes());

const start = async () => {
  try {
    await db.sequelize.authenticate();
    app.listen(process.env.SERVER_PORT, () =>
      console.log(`started on ${process.env.SERVER_PORT}`)
    );
  } catch (err) {
    console.error(err);
  }
};
start();

// queryInterface docs
// https://sequelize.org/master/class/lib/dialects/abstract/query-interface.js~QueryInterface.html
