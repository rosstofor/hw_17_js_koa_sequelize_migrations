const {
  getAddressService,
  getAddressesService,
  checkAddressService,
  createAddressService,
  deleteAddressService,
  updateAddressService,
} = require("../services/address");

const getAddresses = async (ctx) => {
  try {
    const data = await getAddressesService();
    ctx.response.body = { success: true, data };
    ctx.response.status = 200;
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = 400;
  }
};
const getAddress = async (ctx) => {
  try {
    const id = ctx.request.url.split("/").pop();

    const candidate = await checkAddressService(id);
    if (!candidate) {
      return (
        (ctx.response.body = {
          data: { message: `Address with id: ${id} not exist` },
        }),
        (ctx.response.status = 400)
      );
    }

    const data = await getAddressService(id);
    ctx.response.body = { success: true, data };
    ctx.response.status = 200;
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = 400;
  }
};
const createAddress = async (ctx) => {
  try {
    const address = {
      city: ctx.request.body.city,
      street: ctx.request.body.street,
      building: ctx.request.body.building,
    };
    const data = await createAddressService(address);
    ctx.response.status = 201;
    ctx.response.body = { success: true, data };
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = 400;
  }
};
const updateAddress = async (ctx) => {
  try {
    const id = ctx.request.url.split("/").pop();

    const candidate = await checkAddressService(id);
    if (!candidate) {
      return (
        (ctx.response.body = {
          data: { message: `Address with id: ${id} not exist` },
        }),
        (ctx.response.status = 400)
      );
    }

    const address = {
      city: ctx.request.body.city,
      street: ctx.request.body.street,
      building: ctx.request.body.building,
    };
    await updateAddressService(address, id);
    ctx.response.status = 200;
    ctx.response.body = { success: true, data: { message: "Updated" } };
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = 400;
  }
};
const deleteAddress = async (ctx) => {
  try {
    const id = ctx.request.url.split("/").pop();

    const candidate = await checkAddressService(id);
    if (!candidate) {
      return (
        (ctx.response.body = {
          data: { message: `Address with id: ${id} not exist` },
        }),
        (ctx.response.status = 400)
      );
    }

    await deleteAddressService(id);
    ctx.response.status = 200;
    ctx.response.body = { success: true, data: { message: "Deleted" } };
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = 400;
  }
};

module.exports = {
  getAddresses,
  getAddress,
  deleteAddress,
  createAddress,
  updateAddress,
};
